import com.tvc.engine.GameEngine;
import com.tvc.engine.IGameLogic;
import com.tvc.engine.api.events.EventMaster;
import com.tvc.engine.api.events.ListenerHandler;
import com.tvc.engine.api.networking.ClientConnection;
import com.tvc.engine.api.networking.sides.SideHandler;
import com.tvc.game.Game;
import meta.Reference;

public class TheClient {

    public static void main(String args[]) {

        //Set the side to GameClient
        //This will make it easier later to decide weather an instance cares about an event
        SideHandler.setSide(SideHandler.Side.CLIENT);

        //Open the window to the user
        try {
            boolean vSync = true;
            IGameLogic gameLogic = new Game();
            GameEngine gameEngine = new GameEngine("TerminalVelocityGame", 600, 480, vSync, gameLogic);
            gameEngine.start();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }

        //Setup EventMaster
        new EventMaster();

        //Register Listeners
        new ListenerHandler();

        //Create a client Connection
        ClientConnection connection = new ClientConnection(Reference.DEFAULT_HOST, Reference.PORT);
    }

}
