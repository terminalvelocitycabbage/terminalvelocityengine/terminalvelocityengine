import com.tvc.engine.api.events.EventMaster;
import com.tvc.engine.api.events.ListenerHandler;
import com.tvc.engine.api.networking.sides.SideHandler;
import com.tvc.engine.api.util.logger.LoggerHelper;
import com.tvc.server.TVServer;

public class TheServer {

    public static void main(String args[]) {

        //Set the side of ServerConnection
        //This will make it easier later to decide weather an instance cares about an event
        SideHandler.setSide(SideHandler.Side.SERVER);

        //Setup EventMaster
        new EventMaster();

        //Register Listeners
        new ListenerHandler();

        LoggerHelper.info("Starting ServerConnection...");

        //Create a Server
        new TVServer();
    }

}
