import com.tvc.engine.api.events.EventMaster;
import com.tvc.engine.api.events.ListenerHandler;
import com.tvc.engine.api.util.logger.LoggerHelper;
import com.tvc.engine.api.networking.sides.SideHandler;

public class TerminalVelocityEngine {

    public static void main(String args[]) {

        //Set the side to Engine
        //This will make it easier later to decide weather an instance cares about an event
        SideHandler.setSide(SideHandler.Side.ENGINE);

        //Setup EventMaster
        new EventMaster();

        //Register Listeners
        new ListenerHandler();

        LoggerHelper.info("Starting Engine...");
    }

}
