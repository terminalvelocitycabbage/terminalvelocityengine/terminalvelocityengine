module TerminalVelocityEngine {

    requires java.sql;

    requires org.lwjgl;
    requires org.lwjgl.glfw;
    requires org.lwjgl.openal;
    requires org.lwjgl.opengl;
    
    requires SimpleNet;

    requires org.joml;
}