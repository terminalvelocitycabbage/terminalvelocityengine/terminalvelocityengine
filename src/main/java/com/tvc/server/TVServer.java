package com.tvc.server;

import com.tvc.engine.api.events.EventMaster;
import com.tvc.engine.api.networking.ServerConnection;
import meta.Reference;

public class TVServer {

    private int portNumber = Reference.PORT;

    public TVServer() {
        init();
        run();
    }

    public void init() {
        ServerConnection serverConnection = new ServerConnection(portNumber);
    }

    public void run() {
        long lastTime = System.nanoTime();
        double amountOfTicks = 60.0;
        double ns = 1000000000 / amountOfTicks;
        double delta = 0;
        long timer = System.currentTimeMillis();
        int tickRate = 0;
        while(true) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            while(delta >= 1) {
                tick();
                delta--;
            }

            tickRate++;

            if(System.currentTimeMillis() - timer > 1000) {
                timer += 1000;
                tickRate = 0;
            }
        }
    }

    public void tick() {
        //This is where logic is sent back to all clients
        EventMaster.getEventMaster().passEventTick();
    }
}
