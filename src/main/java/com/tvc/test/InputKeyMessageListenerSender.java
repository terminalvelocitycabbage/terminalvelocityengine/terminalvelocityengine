package com.tvc.test;

import com.tvc.engine.api.events.Event;
import com.tvc.engine.api.events.EventListener;
import com.tvc.engine.api.events.EventMaster;
import com.tvc.engine.api.events.input.KeyPressedEvent;
import com.tvc.engine.api.events.input.KeyReleasedEvent;
import com.tvc.engine.api.events.input.MousePressedEvent;
import com.tvc.engine.api.events.input.MouseReleasedEvent;
import com.tvc.engine.api.util.logger.LoggerHelper;
import com.tvc.engine.api.networking.sides.SideHandler;

public class InputKeyMessageListenerSender implements EventListener {

    public InputKeyMessageListenerSender() {
        EventMaster.getEventMaster().addListener(this);
    }

    @Override
    public SideHandler.Side getSide() {
        return SideHandler.Side.CLIENT;
    }

    @Override
    public Class[] getEventTypes() {
        return new Class[]{KeyPressedEvent.class, KeyReleasedEvent.class, MousePressedEvent.class, MouseReleasedEvent.class};
    }

    @Override
    public void passEvent(Event e) {
        if (e.getClass().equals(KeyPressedEvent.class)) {
            LoggerHelper.info("You pressed: " + e.getConcern().toString());
        }
        if (e.getClass().equals(KeyReleasedEvent.class)) {
            LoggerHelper.info("You released: " + e.getConcern().toString());
        }
        if (e.getClass().equals(MousePressedEvent.class)) {
            LoggerHelper.info("You pushed the mouse button: " + e.getConcern().toString());
        }
        if (e.getClass().equals(MouseReleasedEvent.class)) {
            LoggerHelper.info("You released the mouse button: " + e.getConcern().toString());
        }
    }
}
