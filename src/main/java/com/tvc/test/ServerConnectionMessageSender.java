package com.tvc.test;

import com.tvc.engine.api.events.Event;
import com.tvc.engine.api.events.EventListener;
import com.tvc.engine.api.events.EventMaster;
import com.tvc.engine.api.events.networking.PlayerConnectionEvent;
import com.tvc.engine.api.util.logger.LoggerHelper;
import com.tvc.engine.api.networking.sides.SideHandler;

public class ServerConnectionMessageSender implements EventListener {

    public ServerConnectionMessageSender() {
        EventMaster.getEventMaster().addListener(this);
    }

    @Override
    public SideHandler.Side getSide() {
        return SideHandler.Side.SERVER;
    }

    @Override
    public Class[] getEventTypes() {
        return new Class[]{PlayerConnectionEvent.class};
    }

    @Override
    public void passEvent(Event e) {
        LoggerHelper.info(e.getConcern() + " joined the TVServer!");
    }
}
