package com.tvc.engine.client;

public class GameClient {

    private String username;

    public GameClient(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
