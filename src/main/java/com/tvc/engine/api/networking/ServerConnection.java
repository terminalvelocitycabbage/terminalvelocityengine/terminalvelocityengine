package com.tvc.engine.api.networking;

import com.tvc.engine.api.events.EventMaster;
import com.tvc.engine.api.events.ListenerHandler;
import com.tvc.engine.api.events.networking.PlayerConnectionEvent;
import com.tvc.engine.api.networking.sides.SideHandler;
import com.tvc.engine.client.GameClient;
import simplenet.Server;

public class ServerConnection {

    private static ListenerHandler listenerHandler;

    public ServerConnection(int portNumber) {

        //Instantiate a new TVServer.
        Server server = new Server();

        server.bind("localhost", portNumber);

        server.onConnect(client -> {
            //Pass player connection event
            EventMaster.getEventMaster().addEventToPool(new PlayerConnectionEvent(SideHandler.Side.SERVER, new GameClient("username")));

            client.readByteAlways(channel -> {
                switch (channel) {
                    //Connections and disconnections = 1
                    case 1:
                        //Read the client info from the byte array
                        //this uses a future feature of the Networking API

                        //TODO: below
                        //GameClient gc = ByteObjects.fromBytes(client.readBytes());

                        //Fire the connection event with concern: GameClient and it's info.
                }
            });
        });
    }
}
