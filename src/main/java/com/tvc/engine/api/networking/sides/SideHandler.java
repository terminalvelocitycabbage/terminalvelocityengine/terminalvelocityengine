package com.tvc.engine.api.networking.sides;

public class SideHandler {

    public enum Side{
        SERVER, CLIENT, ENGINE;
    }

    private static Side side;

    public static Side getSide() {
        return side;
    }

    public static void setSide(Side side) {
        SideHandler.side = side;
    }
}
