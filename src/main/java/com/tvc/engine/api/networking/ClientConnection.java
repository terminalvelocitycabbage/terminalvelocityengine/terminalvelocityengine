package com.tvc.engine.api.networking;

import com.tvc.engine.api.util.logger.LoggerHelper;
import simplenet.Client;

import java.util.concurrent.atomic.AtomicBoolean;

public class ClientConnection {

    private static AtomicBoolean connected = new AtomicBoolean(false);

    public ClientConnection(String hostName, int portNumber) {
        Client client = new Client();

        client.onConnect(() -> {
            System.out.println(client + " has connected to the server!");
            connected.set(true);
            /*
            try {
                Packet.builder().putByte(1).putBytes(ByteObjects.toBytes(new GameClient("username"))).writeAndFlush(client);
            } catch (IOException e) {
                e.printStackTrace();
            }
            */
        });

        client.preDisconnect(() -> connected.set(false));

        client.postDisconnect(() -> {
            System.out.println(client + " has disconnected from gameservers! Attempting to reconnect");
            while (!connected.getPlain()) {
                try {
                    client.connect(hostName, portNumber);
                } catch (Throwable throwable) {
                    LoggerHelper.error("Failed to reconnect.");
                }
            }
        });

        client.connect(hostName, portNumber);
    }
}
