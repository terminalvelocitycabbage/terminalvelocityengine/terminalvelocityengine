package com.tvc.engine.api.util.logger;

import com.tvc.engine.api.networking.sides.SideHandler;

public class LoggerHelper {

    public static void info(String message) {
        System.out.println("[" + SideHandler.getSide().toString() + "] " + message);
    }

    public static void error(String message) {
        System.err.println("[" + SideHandler.getSide().toString() + " ERROR] " + message);
    }

    public static void warn(String message) {
        System.out.println("[" + SideHandler.getSide().toString() + " WARN] " + message);
    }
}
