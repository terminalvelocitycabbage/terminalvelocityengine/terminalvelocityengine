package com.tvc.engine.api.util.resources;

import java.io.InputStream;
import java.util.Scanner;

public class ResourceLoader {

    public static String loadResource(String fileName) throws Exception {
        String result;
        try (InputStream in = Class.forName(ResourceLoader.class.getName()).getResourceAsStream(fileName);
             Scanner scanner = new Scanner(in, "UTF-8")) {
            result = scanner.useDelimiter("\\A").next();
        }
        return result;
    }
}
