package com.tvc.engine.api.events;

import com.tvc.engine.api.networking.sides.SideHandler;

public interface EventListener {

    SideHandler.Side getSide();

    Class[] getEventTypes();

    void passEvent(Event e);
}
