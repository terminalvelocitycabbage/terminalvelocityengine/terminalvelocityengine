package com.tvc.engine.api.events;

import com.tvc.test.InputKeyMessageListenerSender;
import com.tvc.test.ServerConnectionMessageSender;

public class ListenerHandler {

    public ListenerHandler() {

        //Any event listener needs to bae instantiated here

        //Test
        new ServerConnectionMessageSender();
        new InputKeyMessageListenerSender();
    }
}
