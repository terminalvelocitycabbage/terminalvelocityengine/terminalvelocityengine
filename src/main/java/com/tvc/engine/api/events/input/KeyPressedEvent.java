package com.tvc.engine.api.events.input;

import com.tvc.engine.api.events.Event;
import com.tvc.engine.api.networking.sides.SideHandler;

public class KeyPressedEvent extends Event {

    private SideHandler.Side side;
    private int keyCode;

    public KeyPressedEvent(SideHandler.Side side, int keyCode) {
        this.side = side;
        this.keyCode = keyCode;
    }

    @Override
    public SideHandler.Side getSide() {
        return side;
    }

    @Override
    public Object getConcern() {
        return keyCode;
    }
}
