package com.tvc.engine.api.events;

import java.util.ArrayList;

public class EventMaster {

    private static EventMaster eventMaster;

    private static ArrayList<EventListener> listeners = new ArrayList<>();
    private static ArrayList<Event> eventQueue = new ArrayList<>();

    public EventMaster() { eventMaster = this; }

    public void passEventTick() {
        if (eventQueue.size() < 1) {
            return;
        }
        for (Event event : eventQueue) {
            for (EventListener listener : listeners) {
                //IF the listener cared about the type of event we're giving it
                for (Class classObj : listener.getEventTypes()) {
                    if (classObj.equals(event.getClass())) {
                        listener.passEvent(event);
                    }
                }
            }
        }
        eventQueue.clear();
    }

    public void addListener(EventListener listener) {
        listeners.add(listener);
    }

    public void addEventToPool(Event event) {
        eventQueue.add(event);
    }

    public static EventMaster getEventMaster() {
        return eventMaster;
    }
}
