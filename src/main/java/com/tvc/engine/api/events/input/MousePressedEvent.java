package com.tvc.engine.api.events.input;

import com.tvc.engine.api.events.Event;
import com.tvc.engine.api.networking.sides.SideHandler;

public class MousePressedEvent extends Event {

    private SideHandler.Side side;
    private int mouseCode;

    public MousePressedEvent(SideHandler.Side side, int keyCode) {
        this.side = side;
        this.mouseCode = keyCode;
    }

    @Override
    public SideHandler.Side getSide() {
        return side;
    }

    @Override
    public Object getConcern() {
        return mouseCode;
    }
}
