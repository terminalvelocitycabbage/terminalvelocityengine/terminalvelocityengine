package com.tvc.engine.api.events.networking;

import com.tvc.engine.api.events.Event;
import com.tvc.engine.api.networking.sides.SideHandler;
import com.tvc.engine.client.GameClient;

public class PlayerConnectionEvent extends Event {

    private SideHandler.Side side;
    private GameClient client;

    public PlayerConnectionEvent(SideHandler.Side side, GameClient client) {
        this.side = side;
        this.client = client;
    }

    @Override
    public SideHandler.Side getSide() {
        return side;
    }

    @Override
    public Object getConcern() {
        return client;
    }
}
