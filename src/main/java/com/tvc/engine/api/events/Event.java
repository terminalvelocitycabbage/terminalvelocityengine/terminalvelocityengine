package com.tvc.engine.api.events;

import com.tvc.engine.api.networking.sides.SideHandler;

public abstract class Event {

    public abstract SideHandler.Side getSide();

    public abstract Object getConcern();
}
