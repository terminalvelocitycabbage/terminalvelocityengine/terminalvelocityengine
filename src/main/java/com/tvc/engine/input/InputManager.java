package com.tvc.engine.input;

import com.tvc.engine.api.events.EventMaster;
import com.tvc.engine.api.events.input.KeyPressedEvent;
import com.tvc.engine.api.events.input.KeyReleasedEvent;
import com.tvc.engine.api.events.input.MousePressedEvent;
import com.tvc.engine.api.events.input.MouseReleasedEvent;
import com.tvc.engine.api.networking.sides.SideHandler;
import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFW;

import java.nio.DoubleBuffer;

public class InputManager {

    private static long window;

    private static boolean[] keys = new boolean[GLFW.GLFW_KEY_LAST];
    private static boolean[] mouseButtons = new boolean[GLFW.GLFW_MOUSE_BUTTON_LAST];

    public InputManager(long window) {
        InputManager.window = window;
    }

    //Update key presses
    public static void keyEvents() {

        //update the last frame's key(s) in the arrays && pass an event if needed
        for (int i = 0; i < GLFW.GLFW_KEY_LAST; i++) {
            if (isKeyPressed(i)) {
                EventMaster.getEventMaster().addEventToPool(new KeyPressedEvent(SideHandler.Side.SERVER, i));
            }
            if (isKeyRelesased(i)) {
                EventMaster.getEventMaster().addEventToPool(new KeyReleasedEvent(SideHandler.Side.SERVER, i));
            }
            keys[i] = isKeyDown(i);
        }
        for (int i = 0; i < GLFW.GLFW_MOUSE_BUTTON_LAST; i++) {
            if (isMousePressed(i)) {
                EventMaster.getEventMaster().addEventToPool(new MousePressedEvent(SideHandler.Side.SERVER, i));
            }
            if (isMouseRelesased(i)) {
                EventMaster.getEventMaster().addEventToPool(new MouseReleasedEvent(SideHandler.Side.SERVER, i));
            }
            mouseButtons[i] = isMouseDown(i);
        }
    }

    //Is the specified key bing held down
    public static boolean isKeyDown(int keyCode) {
        return GLFW.glfwGetKey(window, keyCode) == 1; //Not sure why this method doesn't return a bool by default but meh
    }

    //Is the specified mouse button being held down
    public static boolean isMouseDown(int mouseCode) {
        return GLFW.glfwGetMouseButton(window, mouseCode) == 1;
    }

    //if the current frame has the key pressed and the last didn't
    public static boolean isKeyPressed(int keyCode) {
        return isKeyDown(keyCode) && !keys[keyCode];
    }

    //If the current frame doesn't have the key pressed and the last did
    public static boolean isKeyRelesased(int keyCode) {
        return !isKeyDown(keyCode) && keys[keyCode];
    }

    //if the current frame has the mouse button pressed and the last didn't
    public static boolean isMousePressed(int mouseCode) {
        return isMouseDown(mouseCode) && !mouseButtons[mouseCode];
    }

    //If the current frame doesn't have the mouse button pressed and the last did
    public static boolean isMouseRelesased(int mouseCode) {
        return !isMouseDown(mouseCode) && mouseButtons[mouseCode];
    }

    //Get the X coordinate of the mouse
    public static double getMouseX() {
        DoubleBuffer buffer = BufferUtils.createDoubleBuffer(1);
        GLFW.glfwGetCursorPos(window, buffer, null);
        return buffer.get(0);
    }

    //Get the Y coordinate of the mouse
    public static double getMouseY() {
        DoubleBuffer buffer = BufferUtils.createDoubleBuffer(1);
        GLFW.glfwGetCursorPos(window, null , buffer);
        return buffer.get(0);
    }
}
